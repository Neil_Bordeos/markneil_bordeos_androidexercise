package com.example.bordeoscontracttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class HealtInfo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_healt_info)


        val fname = intent.getStringExtra("fname")
        val contact = intent.getStringExtra("contact")
        val city = intent.getStringExtra("city")

        val et1 = findViewById<EditText>(R.id.et1)
        val et2 = findViewById<EditText>(R.id.et2)
        val et3 = findViewById<EditText>(R.id.et3)
        val et4 = findViewById<EditText>(R.id.et4)

        val nextButton2 = findViewById<Button>(R.id.btnNext2)
        nextButton2.setOnClickListener{
            val hiIntent = Intent(this, Confirmation::class.java)

            hiIntent.putExtra("fname",fname.toString())
            hiIntent.putExtra("contact",contact.toString())
            hiIntent.putExtra("city",city.toString())

            hiIntent.putExtra("et1", et1.text.toString())
            hiIntent.putExtra("et2", et2.text.toString())
            hiIntent.putExtra("et3", et3.text.toString())
            hiIntent.putExtra("et4", et4.text.toString())

            startActivity(hiIntent)
        }
    }
}