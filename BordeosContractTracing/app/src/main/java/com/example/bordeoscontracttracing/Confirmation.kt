package com.example.bordeoscontracttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Confirmation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        val textView1 = findViewById<TextView>(R.id.textView1)
        val textView2 = findViewById<TextView>(R.id.textView2)
        val textView3 = findViewById<TextView>(R.id.textView3)
        val textView4 = findViewById<TextView>(R.id.textView4)
        val textView5 = findViewById<TextView>(R.id.textView5)
        val textView6 = findViewById<TextView>(R.id.textView6)
        val textView7 = findViewById<TextView>(R.id.textView7)
        val q1 = intent.getStringExtra("fname")
        val q2 = intent.getStringExtra("contact")
        val q3 = intent.getStringExtra("city")
        val q4 = intent.getStringExtra("et1")
        val q5 = intent.getStringExtra("et2")
        val q6 = intent.getStringExtra("et3")
        val q7 = intent.getStringExtra("et4")
        textView1.text = q1
        textView2.text = q2
        textView3.text = q3
        textView4.text = q4
        textView5.text = q5
        textView6.text = q6
        textView7.text = q7

        val nextButton2 = findViewById<Button>(R.id.btnNext2)
        nextButton2.setOnClickListener {
            val intent = Intent(this, Success::class.java)
            startActivity(intent)


        }
    }
}