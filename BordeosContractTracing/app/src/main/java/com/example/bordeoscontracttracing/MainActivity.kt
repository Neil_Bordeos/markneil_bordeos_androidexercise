package com.example.bordeoscontracttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val etFname = findViewById<EditText>(R.id.etFname)
        val etContact = findViewById<EditText>(R.id.etContact)
        val etCity = findViewById<EditText>(R.id.etCity)
        val nextButton = findViewById<Button>(R.id.btnNext)
        nextButton.setOnClickListener{
            val intent = Intent(this,HealtInfo::class.java)
            intent.putExtra("fname", etFname.text.toString())
            intent.putExtra("contact", etContact.text.toString())
            intent.putExtra("city", etCity.text.toString())
            startActivity(intent)
        }
    }
}